#include <stdio.h>
#include <stdlib.h>

#define STB_IMAGE_IMPLEMENTATION

#include "stb_image/stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION

#include "stb_image/stb_image_write.h"

void yoda_to_gray();

void road_gray();

void yoda_to_black_white_single_thresholding();

void yoda_to_black_white_double_thresholding();

void yoda_enhance_contrast();

int main() {
    printf("Image conversion program");
    yoda_to_gray();
    yoda_to_black_white_single_thresholding();
    yoda_to_black_white_double_thresholding();
    yoda_enhance_contrast();
    road_gray();
}

void yoda_to_gray() {
    int width, height, channels;
    unsigned char *img = stbi_load("yoda.jpeg", &width, &height, &channels, 0);
    if (img == NULL) {
        printf("Unable to allocate memory for the gray image.\n");
        exit(1);
    }
    size_t img_size = width * height * channels;
    int gray_channels = 1;
    size_t gray_img_size = width * height * gray_channels;
    unsigned char *img_gray = malloc(gray_img_size);
    for (unsigned char *p = img, *pg = img_gray; p != img + img_size; p += channels, pg += gray_channels) {
        *pg = (uint8_t) ((*p + *(p + 1) + *(p + 2)) / 3.0);
//        if (channels == 4) {
//            *(pg + 1) = *(p + 3);
//        }
    }
    stbi_write_jpg("yoda_to_gray.jpeg", width, height, gray_channels, img_gray, 100);
    free(img_gray);
    stbi_image_free(img);
    printf("\nconverted to gray successfully");
}

void yoda_to_black_white_single_thresholding() {
    int width, height, channels_gray = 1, channels;
    unsigned char *img_gray = stbi_load("yoda_to_gray.jpeg", &width, &height, &channels, 0);
    if (img_gray == NULL) {
        printf("Unable to allocate memory for the gray image.\n");
        exit(1);
    }
    int gray_img_size = width * height * channels;
    unsigned char threshold = 128, new_threshold, *p; // initial guess, pointer to image pixels
    unsigned long int group_1_sum, group_2_sum; // group 1 > threshold
    unsigned int group_1_num, group_2_num, group_1_ava, group_2_ava;
    while (1) {
        group_1_sum = 0, group_2_sum = 0, group_1_num = 0, group_2_num = 0;
        for (p = img_gray; p != img_gray + gray_img_size; p += channels) {
            if (*p > threshold) {
                group_1_sum += *p;
                group_1_num++;
            } else {
                group_2_sum += *p;
                group_2_num++;
            }
        }
        group_1_ava = group_1_sum / group_1_num;
        group_2_ava = group_2_sum / group_2_num;
        new_threshold = (group_1_ava + group_2_ava) / 2;
        if (new_threshold == threshold)
            break;
        threshold = new_threshold;
    }
    unsigned char *img_single_threshold = malloc(gray_img_size);
    unsigned char *p_img_single_threshold = img_single_threshold;
    for (p = img_gray; p != img_gray + gray_img_size; p += channels_gray, p_img_single_threshold += channels_gray) {
        if (*p > threshold)
            *p_img_single_threshold = 255;
        else
            *p_img_single_threshold = 0;
    }
    stbi_write_jpg("yoda_single_threshold.jpeg", width, height, channels, img_single_threshold, 100);
    stbi_image_free(img_gray);
    free(img_single_threshold);
    printf("\nconverted to black and white using single threshold successfully");
}

void yoda_to_black_white_double_thresholding() {
    int width, height, channels, channels_gray = 1;
    unsigned char *img_gray = stbi_load("yoda_to_gray.jpeg", &width, &height, &channels, 0);
    if (img_gray == NULL) {
        printf("Unable to allocate memory for the gray image.\n");
        exit(1);
    }
    size_t gray_img_size = width * height * channels;
    unsigned char *pointer_img_double_threshold;
    unsigned char threshold = 128, new_threshold, *p; // initial guess, pointer to image pixels
    unsigned long int group_1_sum, group_2_sum; // group 1 > threshold
    unsigned int group_1_num, group_2_num, group_1_ava, group_2_ava;
    unsigned char low_bound = 100, hi_bound = 155;
    unsigned char *img_double_threshold = malloc(gray_img_size);
    pointer_img_double_threshold = img_double_threshold;
    while (1) {
        group_1_sum = 0, group_2_sum = 0, group_1_num = 0, group_2_num = 0;
        for (p = img_gray; p != img_gray + gray_img_size; p += channels) {
            if (*p > low_bound && *p < hi_bound) {
                if (*p > threshold) {
                    group_1_sum += *p;
                    group_1_num++;
                } else {
                    group_2_sum += *p;
                    group_2_num++;
                }
            }
        }
        group_1_ava = group_1_sum / group_1_num;
        group_2_ava = group_2_sum / group_2_num;
        new_threshold = (group_1_ava + group_2_ava) / 2;
        if (new_threshold == threshold)
            break;
        threshold = new_threshold;
    }
    for (p = img_gray;
         p != img_gray + gray_img_size; p += channels_gray, pointer_img_double_threshold += channels_gray) {
        if (*p > threshold)
            *pointer_img_double_threshold = 255;
        else
            *pointer_img_double_threshold = 0;
    }
    stbi_write_jpg("yoda_double_threshold.jpeg", width, height, channels, img_double_threshold, 100);
    stbi_image_free(img_gray);
    free(img_double_threshold);
    printf("\nconverted to black and white using double threshold successfully");
}

void yoda_enhance_contrast() {
    int width, height, channels, channels_gray = 1;
    unsigned char *img_gray = stbi_load("yoda_to_gray.jpeg", &width, &height, &channels, 0);
    if (img_gray == NULL) {
        printf("Unable to allocate memory for the gray image.\n");
        exit(1);
    }
    size_t gray_img_size = width * height * channels;
    unsigned char *p, *p_img_enhances_contrast;
    unsigned long int pixels_num = height * width;
    int n; // numerate all possible shades
    unsigned int number_of_each_intensity[256];
    for (n = 0; n < 256; n++) {
        number_of_each_intensity[n] = 0;
    }
    double probability_of_each_intensity[256];
    double cumulative_probability[256];
    for (p = img_gray; p != img_gray + gray_img_size; p += channels) {
        for (n = 0; n < 256; n++) {
            if (*p == n) {
                number_of_each_intensity[n]++;
                break;
            }
        }
    }
    for (n = 0; n < 256; n++) {
        probability_of_each_intensity[n] = (double) number_of_each_intensity[n] / pixels_num;
    }
    cumulative_probability[0] = probability_of_each_intensity[0];
    for (n = 1; n < 256; n++) {
        cumulative_probability[n] = cumulative_probability[n - 1] + probability_of_each_intensity[n];
    }
    unsigned char *img_enhanced_contrast = malloc(gray_img_size);
    p_img_enhances_contrast = img_enhanced_contrast;
    for (p = img_gray; p != img_gray + gray_img_size; p += channels_gray, p_img_enhances_contrast += channels_gray) {
        for (n = 0; n < 256; n++) {
            if (*p == n)
                *p_img_enhances_contrast = (int) (cumulative_probability[n] * 255);
        }
    }
    stbi_write_jpg("yoda_enhanced_contrast.jpeg", width, height, channels, img_enhanced_contrast, 100);
    stbi_image_free(img_gray);
    free(img_enhanced_contrast);
    printf("\nenhanced contrast successfully");
}

void road_gray() {
    int width, height, channels;
    unsigned char *img = stbi_load("road.jpg", &width, &height, &channels, 0);
    if (img == NULL) {
        printf("Unable to allocate memory for the gray image.\n");
        exit(1);
    }
    size_t img_size = width * height * channels;
    int gray_channels = 1;
    size_t gray_img_size = width * height * gray_channels;
    unsigned char *gray_img = malloc(gray_img_size);
    if (gray_img == NULL) {
        printf("Unable to allocate memory for the gray image.\n");
        exit(1);
    }
    for (unsigned char *p = img, *pg = gray_img; p != img + img_size; p += channels, pg += gray_channels) {
        *pg = (uint8_t) ((*p + *(p + 1) + *(p + 2)) / 3.0);
        if (channels == 4) {
            *(pg + 1) = *(p + 3);
        }
    }
    stbi_write_jpg("road_gray.jpg", width, height, gray_channels, gray_img, 100);
    stbi_image_free(img);
    stbi_image_free(gray_img);
    printf("\nconverted road to gray successfully");
}
